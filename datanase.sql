-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para supermarket
DROP DATABASE IF EXISTS `supermarket`;
CREATE DATABASE IF NOT EXISTS `supermarket` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci */;
USE `supermarket`;

-- Volcando estructura para tabla supermarket.clientes
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `cod_cli_pk` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nom_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ape_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `sexo_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cc_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tel_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nit_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `email_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `dir_cli` varchar(300) COLLATE utf8mb4_spanish2_ci NOT NULL,
  PRIMARY KEY (`cod_cli_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla supermarket.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`cod_cli_pk`, `nom_cli`, `ape_cli`, `sexo_cli`, `cc_cli`, `tel_cli`, `nit_cli`, `email_cli`, `dir_cli`) VALUES
	('1', 'andres', 'mendoza', 'm', '1012522525', '3142402307', '252525', 'email@email.com', 'dir');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla supermarket.detallefacturas
DROP TABLE IF EXISTS `detallefacturas`;
CREATE TABLE IF NOT EXISTS `detallefacturas` (
  `num_fac_fk` int(11) DEFAULT NULL,
  `cod_pro_fk` int(11) DEFAULT NULL,
  `detalle_fac` varchar(512) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `vr_unitario` int(11) DEFAULT NULL,
  `vr_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla supermarket.detallefacturas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `detallefacturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefacturas` ENABLE KEYS */;

-- Volcando estructura para tabla supermarket.facturas
DROP TABLE IF EXISTS `facturas`;
CREATE TABLE IF NOT EXISTS `facturas` (
  `num_fac_pk` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cod_cli_fk` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL DEFAULT '0',
  `nit_cli` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `subtotal` varchar(512) COLLATE utf8mb4_spanish2_ci NOT NULL DEFAULT '0',
  `iva` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL DEFAULT '0',
  `total` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL DEFAULT '0',
  `fecha_fac` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`num_fac_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla supermarket.facturas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;

-- Volcando estructura para tabla supermarket.productos
DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `cod_pro_pk` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `descripcion_pro` varchar(512) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `precio_pro` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  `stock` varchar(50) COLLATE utf8mb4_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`cod_pro_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- Volcando datos para la tabla supermarket.productos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`cod_pro_pk`, `descripcion_pro`, `precio_pro`, `stock`) VALUES
	('001', 'producto1', '1500', '5'),
	('002', 'producto2', '2000', '10'),
	('003', 'producto3	', '7000', '10');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
